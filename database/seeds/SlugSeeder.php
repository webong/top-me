<?php

use Illuminate\Database\Seeder;
use App\Campaign;

class SlugSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Campaign::all() as $campaign) {
        	$campaign->slug = $this->getslug($campaign);
        	$campaign->save();
        }
    }

    public function getslug($campaign)
    {
     $remove      =  array("?","%","$","@","*", "\/", "\\", "!", "(", ")", "#", "^", "+", "=", '.', ",", "'", ">", "<", "]", "[", '{', "}", ";", ":", '"' );
  	 $string      = str_replace( $remove , "", $campaign->title);
     $str         = strtolower($string);
     $rep         = preg_replace('/\s+/', '-', $str);
     $characters  = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // generate a pin based the timestamp

        $pin = $campaign->created_at->timestamp;

         $slug = $rep. '-' . str_shuffle($pin);
       
       return $slug;
    }
}
