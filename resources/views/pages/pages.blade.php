@extends('layouts.app')


@section('content')

<div class="container">

  <div class="row my-5">

    <div class="col-4 privacy-tab">
      @include('pages.pages-sidebar')
    </div>

    <div class="col-8 privacy-content">
      <div class="tab-content my-4 px-5" id="v-pills-tabContent">
        <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
          @include('pages.privacy')
        </div>
        <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
          @include('pages.service')
        </div>
        <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
          @include('pages.conditions')
        </div>
      </div>
    </div>
  </div>

</div>
@endsection
