@extends('layouts.app')

@section('content')
<section id="hero">
  <div class="jumbotron jumbotron-fluid hero text-white">
    <div class="container">
      <h1 class="display-5"> Create Campaigns,<br>Easy, Flexible and <br> Secure.</h1><br><br>
      <p class="">TopMe helps you to create a campaign. Ut enim<br> ad minm veniam,quis nostrud exercitation<br>ullamco laboris.</p>
      <button type="button" class="btn btn-light px-5">Get Started</button>
      <button type="button" class="btn btn-secondary ml-5 px-3 btn-know">Know Us</button>
    </div>
  </div>
</section>
@endsection
