<style type="text/css">
  .cloudinary-button{
    display: none
  }
  .cloudinary-thumbnails{
    display: none;
  }
</style>
<div class="container container-fluid pb-5">
  <div class="row mt-3 mb-5 mt-sm-2 mb-sm-5 my-md-4 my-lg-5">
    <div class="col-0 col-sm-0 col-md-4 col-lg-4 col-xl-4 bg-waring d-none d-sm-none d-md-flex d-lg-flex d-xl-flex">
      <div class="nav flex-column nav-pills my-4 bg-succss" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <a class="nav-lin text-dark f-24 my-3" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Basic Information</a>
        <!-- <a class="nav-link v-nav my-3" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Terms of Service</a> -->
      </div>
    </div>
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 bgdangr px-0">
      <div class="tab-content my-0 bg-succss" id="v-pills-tabContent">
        <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">

          <div class="container bg-uccess">
            <form method="POST" enctype="multipart/form-data" action="{{url('/update-bio')}}" class=" bg-dnger pl-0 pl-sm-0 pl-md-5 pl-lg-5">
              @csrf
              <div class="border  p-4 mb-5">
                <div class="d-flex justify-content-center justify-content-sm-center justify-content-md-center justify-content-lg-center justify-content-xl-center mb-3">
                  <div class="d-block">
                    <input type="hidden" name="picture" value="{{ Auth::user()->picture }}" id="pic">
                    <div class="">
                      <img src="{{ Auth::user()->picture == null ? '/img/test-img.png' : Auth::user()->picture }}" class="img-fluid rouded chimage" style="max-height: 200px!important; border-radius:50%;" id="profileImage" style=""  alt="Cinque Terre">
                      
                    </div>
                    <div class="text-center pt-3">
                      <div for="file-upload-btn" id="fileSelect" class="f-16 text-purple" style="cursor:pointer">
                        <i class="fas fa-cloud-upload-alt mt-2 mb-0 mr-2"></i>Upload Image

                      </div>
                      <button id="upload_widget" style="display: none">Open Widget</button>
                      {{-- <input type="file" id="fileElem" name="image" accept="image/*" value="{{Auth::user()->picture}}" id="file-upload-btn" style="display: none;" onchange="imageHandler(this.files)"> --}}
                    </div>
                  </div>
                </div>
                @php
                  $name = explode(' ', Auth::user()->name);
                  $num = count($name);
                @endphp
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="inputEmail4" class="f-16">First Name</label>
                    <input type="text" value="{{$name[0]}}" name="fname" class="form-control form-control-lg rounded-0" placeholder=" ">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="inputPassword4" class="f-16">Last Name</label>
                    <input type="text" name="lname" value="{{$num>1 ? $name[1]: ' '}}" class="form-control form-control-lg rounded-0" placeholder=" ">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputAddress" class="f-16">Occupation</label>
                  <input type="text" value="{{Auth::user()->occupation}}" name="occupation" class="form-control form-control-lg rounded-0" placeholder="Actor, Mentor, Public Speaker">
                </div>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="inputState" class="f-16">Family</label>
                    <select id="inputState" name="marital" class="form-control form-control-lg rounded-0">
                      <option {{Auth::user()->marital_status == 'Single'? 'selected': ''}} >Single</option>
                      <option {{Auth::user()->marital_status == 'Married'? 'selected': ''}}>Married</option>
                      <option {{Auth::user()->marital_status == 'Divorced'? 'selected': ''}}>Divorced</option>
                      <option {{Auth::user()->marital_status == 'Prefer not to disclose'? 'selected': ''}}>Prefer not to disclose</option>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="inputPassword4" class="f-16">Location</label>
                    <input type="text" value="{{Auth::user()->location}}" name="location" class="form-control form-control-lg rounded-0" placeholder="">
                  </div>
                </div>
              </div>

              <div class="border  p-4 mt-5">
                <div class="form-group">
                  <label for="exampleFormControlTextarea1" class="f-16">Description</label>
                  <textarea class="form-control form-control-lg rounded-0" id="exampleFormControlTextarea1" value="{{Auth::user()->objective}} " name="bio" placeholder="Tell us about yourself" rows="5"> {{Auth::user()->objective}} </textarea>
                </div>
              </div>

              <div class="d-flex justify-content-center mt-5">
                <button type="submit" class="btn btn-purple px-5 mx-4">Save</button>
                <button type="button" class="btn bg-transparent bg-purple text-purple px-5 mx-4">Cancel</button>
              </div>
            </form>
          </div>

        </div>
        <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
          @include('pages.service')
        </div>
      </div>
    </div>

  </div>
</div>

{{-- <script type="text/javascript">
  var fileSelect = document.querySelector('#fileSelect');
  var fileElem = document.querySelector('#fileElem');
  var img = document.querySelector('#profileImage');
  fileSelect.addEventListener('click', function(){
    fileElem.click();
  })
  function imageHandler(files){
    if (files[0].type.startsWith('image/')) {
      img.file = files[0];

      var reader = new FileReader();


      reader.onload = (function(a){
        return function(e){a.src = e.target.result}
      })(img)

      reader.readAsDataURL(files[0]);
    }
  }
</script> --}}
<script type="text/javascript">
  var img = document.querySelector('#profileImage');
  var fileSelect = document.querySelector('#fileSelect');
  var picInput = document.querySelector('#pic');
  fileSelect.addEventListener('click', function(){
    document.querySelectorAll('.cloudinary-button')[1].click();
  }) 
  $('#upload_widget').cloudinary_upload_widget(
    { cloud_name: 'djz6ymuuy', upload_preset: 'kt98ybsg', 
      cropping: 'server', folder: 'test', cropping_aspect_ratio:'1', gravity:'custom', multiple:'false', cropping_coordinates_mode:'custom' },
    function(error, result) { 
      img.src = result[0].url;
      pic.value = result[0].url;
      console.log(result[0].url) 
    });
</script>
