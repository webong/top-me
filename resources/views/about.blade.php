@extends('layouts.app')

@section('content')

<!-- <div id="about-banner" class="bg-danger">
  <div class="container about-content">

      <div class="d-flex justify-content-center align-items-end video-section-1">
        <div class="row video-section" >
          <div class="col-12 col-md-6">
            <div class="embed-responsive embed-responsive-16by9 about-video">
              <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/tCjZmnFcqoU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
          </div>

          <div class="col-12 col-md-6 px-3 px-sm-3 px-md-5 px-lg-5 px-xl-5 p-20">
            <p class="about-video-text ">Topme is about giving back, raising lots of money for great causes, and having the most fun in the world while doing it.</p>
          </div>

       </div>
      </div>

  </div>
</div> -->
<!-- <div class="row" style="margin-top:30vh;">
  <div class="col-12 col-sm-12 col-md-6 bg-daner px-2">
    <div class="about-video embed-responsive embed-responsive-16by9">
      <iframe width="560" height="416" src="https://www.youtube-nocookie.com/embed/tCjZmnFcqoU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>
  </div>
  <div class="col-12 col-sm-12 col-md-6 bg-warnig  px-3 px-sm-3 px-md-5 px-lg-5 px-xl-5 d-flex align-items-end">
    <p class="about-video-text ">Topme is about giving back, raising lots of money for great causes, and having the most fun in the world while doing it.</p>
  </div>
</div> -->

<div class="jumbotron jumbotron-fluid d-flex align-items-center mb-0 mb-md-4 mb-lg-4"  id="about-banner">
  <div class="container container-fluid">
    <p class="f-40 font-weight-bold text-white text-center">About Topme</p>
  </div>
</div>

<section>
  <div class="d-flex justify-content-center bg-dger mt-0">
    <div class="row  bg-der w-100 px-0 vh-140"  style="background: url('img/about-vec.svg'); background-size:contain">
      <div class="col-12 col-sm-12 col-md-6 col-lg-6 d-flex align-items-start align-items-sm-start align-items-md-center align-items-lg-center">
        <div class="d-block mx-0 mx-md-5 mx-lg-5 pt-5 pt-sm-5 pt-md-0 pt-lg-0">
          <p class="font-weight-bold f-18 mb-0 d-none d-md-flex">About Topme</p>
          <p class="f-42 font-weight-bold text-center text-md-left text-lg-left">How We Work And <br> What We Do</p>
          <p class="f-24 font-weight-bold">Topme is about giving back, raising lots of money for great causes,
            and having the most fun in the world while doing it.</p>
          <p class="f-18">m ipsum dolor sit amet, consectetuer adipiscing elit.
            Nulla pulvinar eleifend sem. Suspendisse nisl. Nullam eget nisl. Morbi scelerisque luctus velit.
            Vestibulum erat nulla, ulla. Morbi scelerisque luctus velit. Vestibulum erat nulla, ulla</p>
          <div class="d-block mb-0 text-center text-md-left text-lg-left">
            <a href="#" class="text-purple f-24 font-weight-bold">Want to contribute to projects?</a><br><br>
            <a href="{{ url('/impact-agents')}}" class="text-dark f-16 text-underline border-bottom">View our SIAs</a>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-6 col-lg-6 b-success  d-none d-md-flex" style=" background: url('img/about-imgs.svg')">

      </div>
    </div>
  </div>
</section>

<section>
  <div class="d-flex justify-content-center mt-5 bg-nger">
    <div class="row  bg-der vh-65" style="width:90%!important;">
      <div class="col-12 col-sm-12 col-md-4 col-lg-4 bg-daer px-0 pt-0 pt-md-5 pt-lg-5" style="background: url('img/shapes-mission-alt.svg')">
        <!-- card -->
        <p class="f-42 font-weight-bold text-center text-md-left text-lg-left">Mission & Vision</p>
      </div>
      <div class="col-12 col-sm-12 col-md-8 col-lg-8 f-18 px-0 pt-0 pt-sm-0 pt-md-5 pt-lg-5">
        <div class=" mb-5 pt-0 pt-sm-0 pt-md-4 pt-lg-4">
          <p class="text-purple mb-0" style="font-weight: 700">Mission </p>
          <p class="text-justify f-16">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            Nulla pulvinar eleifend sem. Suspendisse nisl. Nullam eget nislLorem </p>
        </div>
        <div class=" mt-3">
          <p class="text-purple mb-0" style="font-weight: 700">Vision </p>
          <p class="text-justify f-16">m ipsum dolor sit amet, consectetuer adipiscing elit. Nulla pulvinar eleifend sem.
             Suspendisse nisl. Nullam eget nisl. Morbi scelerisque luctus velit.
             Vestibulum erat nulla, ulla. Morbi scelerisque luctus velit. Vestibulum erat nulla, ulla</p>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="about-video embed-responsive embed-responsive-16by9" style="margin-top:45px">
    <iframe width="560" height="416" src="https://www.youtube-nocookie.com/embed/tCjZmnFcqoU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
  </div>
</section>

<!-- <h5 class="f-24 font-weight-bold my-2 my-sm-2 my-md-5 my-lg-5 my-xl-5 text-center">Partners</h5> -->

<div class="container container-fluid px-0 px-sm-0 px-md-5 px-lg-5  d-none d-sm-none d-md-block d-lg-block" style="margin-top:133px">
  <p class="f-42 text-center" style="margin-bottom:42px">Partners</p>
  <div class="d-block d-sm-block d-md-flex d-lg-flex justify-content-around"  style="margin-bottom:60px">
    <img class="card-img img-fluid rounded-0" src="../img/Partners_Boston.svg" alt="Boston">
    <img class="card-img rounded-0" src="../img/Partners_American.svg" alt="Red Cross">
    <img class="card-img rounded-0" src="../img/Partners_Tough.svg" alt="Tough Mudder">
    <img class="card-img rounded-0" src="../img/Partners_Viacom.svg" alt="Viacom">
  </div>
  <div class="d-block d-sm-block d-md-flex d-lg-flex justify-content-around justify-content-around  px-0 px-sm-0 px-md-5 px-lg-5"  style="margin-bottom:116px">
    <img class="card-img rounded-0" src="../img/Partners_Quicken.svg" alt="Boston">
    <img class="card-img rounded-0" src="../img/Partners_Unicef.svg" alt="Red Cross">
    <img class="card-img rounded-0" src="../img/Partners_StandUp.svg" alt="Tough Mudder">
  </div>
</div>

<div class="container container-fluid px-0 px-sm-0 px-md-5 px-lg-5 d-block d-sm-block d-md-none d-lg-none">
  <p class="f-42 text-center mb-3">Partners</p>
  <div class="row">
    <div class="col-4">
      <img class="card-img img-fluid rounded-0" src="../img/Partners_Boston.svg" alt="Boston">
    </div>
    <div class="col-4">
      <img class="card-img img-fluid rounded-0" src="../img/Partners_American.svg" alt="Red Cross">
    </div>
    <div class="col-4">
      <img class="card-img img-fluid rounded-0" src="../img/Partners_Tough.svg" alt="Tough Mudder">
    </div>
    <div class="col-4">
      <img class="card-img img-fluid rounded-0" src="../img/Partners_Viacom.svg" alt="Viacom">
    </div>
    <div class="col-4">
      <img class="card-img img-fluid rounded-0" src="../img/Partners_Quicken.svg" alt="Boston">
    </div>
    <div class="col-4">
      <img class="card-img img-fluid rounded-0" src="../img/Partners_Unicef.svg" alt="Red Cross">
    </div>
    <div class="col-4">
      <img class="card-img img-fluid rounded-0" src="../img/Partners_StandUp.svg" alt="Tough Mudder">
    </div>
  </div>
</div>



@endsection
