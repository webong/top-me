<div class="modal fade bd-example-modal-lg mt-3 " tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<style>
         .custom-modal{
         	border-top: 10px solid  #490F83; 
         	border-top-left-radius: 12px; 
         	border-top-right-radius: 12px;
         }
		
		@media (min-width: 768px) {
		  .modal-xl {
		    width: 90%;
		   max-width:1200px;
		  }
		}

	   .container {
		  max-width: 960px;
		}

       .lh-condensed { line-height: 1.25; }
	</style>
  <div class="modal-dialog modal-xl custom-modal">
    <div class="modal-content">
      <div class="container-fluid">
      	<div class="modal-header" style="border:none;">
	        {{-- <h3 class="modal-title">ENTER YOUR DONATION</h3> --}}
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
        </div>
        <div class="container">
        	 <div class="text-center">
		        <img class="d-block mx-auto mb-2" src="{{ asset('/img/Topme Logo.png')}}" alt="" width="150" height="100">
		        <h2 class="text-left">Enter Your Donation</h2>
		        {{-- <p class="lead">Below is an example form built entirely with Bootstrap's form controls. Each required form group has a validation state that can be triggered by attempting to submit the form without completing it.</p> --}}
		      </div>
        	<div class="row">
	        <div class="col-md-4 order-md-2 mb-4">
	          <h4 class="d-flex justify-content-between align-items-center mb-3">
	            <span class="text-muted">{{$campaign->title}}</span>
	            <span class="badge badge-secondary badge-pill">Topme</span>
	          </h4>
	          <ul class="list-group mb-3">
	            <li class="list-group-item d-flex justify-content-between lh-condensed">
	              <div>
	                <h6 class="my-0">Product name</h6>
	                <small class="text-muted">Brief description</small>
	              </div>
	              <span class="text-muted">$12</span>
	            </li>
	            <li class="list-group-item d-flex justify-content-between lh-condensed">
	              <div>
	                <h6 class="my-0">Second product</h6>
	                <small class="text-muted">Brief description</small>
	              </div>
	              <span class="text-muted">$8</span>
	            </li>
	            <li class="list-group-item d-flex justify-content-between lh-condensed">
	              <div>
	                <h6 class="my-0">Third item</h6>
	                <small class="text-muted">Brief description</small>
	              </div>
	              <span class="text-muted">$5</span>
	            </li>
	            <li class="list-group-item d-flex justify-content-between bg-light">
	              <div class="text-success">
	                <h6 class="my-0">Promo code</h6>
	                <small>EXAMPLECODE</small>
	              </div>
	              <span class="text-success">-$5</span>
	            </li>
	            <li class="list-group-item d-flex justify-content-between">
	              <span>Total (USD)</span>
	              <strong>$20</strong>
	            </li>
	          </ul>

	          <form class="card p-2">
	            <div class="input-group">
	              <input type="text" class="form-control" placeholder="Promo code">
	              <div class="input-group-append">
	                <button type="submit" class="btn btn-secondary">Redeem</button>
	              </div>
	            </div>
	          </form>
	        </div>

	        <div class="col-md-8 order-md-1">
	          <h4 class="mb-3"></h4>
	          <form class="needs-validation" action="/donate" method="POST" novalidate>
	          	@csrf

				<input type="hidden" name="cart_id" value="{{$campaign->id}}">
				 {{-- <input type="hidden" name="email" value="{{Auth::user()->email}}"> --}} {{-- required --}}
			    <input type="hidden" name="orderID" value="4765">
			    {{-- <input type="hidden" name="amount" value="{{$total.'00'}}"> --}} {{-- required in kobo --}}
			    <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}"> 
			     <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> 
	            <div class="row">
	              <div class="col-md-6 mb-3">
	                {{-- <label for="firstName">First name</label> --}}
	                <input type="number" class="form-control" name="amount" placeholder="ENTER AMOUNT" value="" required style="height: 70px;">
	                <div class="invalid-feedback">
	                  
	                </div>
	              </div>
	              {{-- <div class="col-md-6 mb-3">
	                <label for="lastName">Last name</label>
	                <input type="text" class="form-control" id="lastName" placeholder="" value="" required>
	                <div class="invalid-feedback">
	                  Valid last name is required.
	                </div>
	              </div> --}}
	            </div>
                @auth
	            <div class="mb-3">
	              <label for="username">Name</label>
	              <div class="input-group">
	                <div class="input-group-prepend">
	                  <span class="input-group-text">@</span>
	                </div>
	                <input type="text" class="form-control" id="username" placeholder="Name" required disabled>
	                <div class="invalid-feedback" style="width: 100%;">
	                  Your username is required.
	                </div>
	              </div>
	            </div>

	            <div class="mb-3">
	              <label for="email">Email <span class="text-muted">(Optional)</span></label>
	              <input type="email" class="form-control" id="email" name="email" placeholder="you@example.com">
	              <div class="invalid-feedback">
	                Please enter a valid email address for shipping updates.
	              </div>
	            </div>
               @else
                 <div class="mb-3">
	              <label for="email">Email <span class="text-muted">(Optional)</span></label>
	              <input type="email" class="form-control" id="email" name="email" placeholder="you@example.com">
	              <div class="invalid-feedback">
	                Please enter a valid email address for shipping updates.
	              </div>
	            </div>

               @endauth
	            {{-- <div class="mb-3">
	              <label for="address">Address</label>
	              <input type="text" class="form-control" id="address" placeholder="1234 Main St" required>
	              <div class="invalid-feedback">
	                Please enter your shipping address.
	              </div>
	            </div>

	            <div class="mb-3">
	              <label for="address2">Address 2 <span class="text-muted">(Optional)</span></label>
	              <input type="text" class="form-control" id="address2" placeholder="Apartment or suite">
	            </div> --}}

	            <div class="row">
	              <div class="col-md-5 mb-3">
	                <label for="country">Country</label>
	                <select class="custom-select d-block w-100" id="country" required>
	                  <option value="">Choose...</option>
	                  <option>United States</option>
	                </select>
	                <div class="invalid-feedback">
	                  Please select a valid country.
	                </div>
	              </div>
	              <div class="col-md-4 mb-3">
	                <label for="state">State</label>
	                <select class="custom-select d-block w-100" id="state" required>
	                  <option value="">Choose...</option>
	                  <option>California</option>
	                </select>
	                <div class="invalid-feedback">
	                  Please provide a valid state.
	                </div>
	              </div>
	              <div class="col-md-3 mb-3">
	                <label for="zip">Zip</label>
	                <input type="text" class="form-control" id="zip" placeholder="" required>
	                <div class="invalid-feedback">
	                  Zip code required.
	                </div>
	              </div>
	            </div>
	            <hr class="mb-4">
	           {{--  <div class="custom-control custom-checkbox">
	              <input type="checkbox" class="custom-control-input" id="same-address">
	              <label class="custom-control-label" for="same-address">Shipping address is the same as my billing address</label>
	            </div>
	            <div class="custom-control custom-checkbox">
	              <input type="checkbox" class="custom-control-input" id="save-info">
	              <label class="custom-control-label" for="save-info">Save this information for next time</label>
	            </div> --}}
	            <hr class="mb-4">

	            <h4 class="mb-3">Payment</h4>

	            <div class="d-block my-3">
		            <div class="form-check d-flex d-inline custom-control custom-radio">
					  <input class="form-check-input" style="height: 20px; width:5%;" type="radio" name="status" id="status" value="debit" checked>
					  <label class="form-check-label" for="status" style="padding-left: 10px;">
					    Debit card
					  </label>
					</div>
					<div class="form-check d-flex d-inline custom-control custom-radio">
					  <input class="form-check-input" style="height: 20px; width:5%; " type="radio" name="status" id="status" value="paypal">
					  <label class="form-check-label" for="status" style="padding-left:  10px;">
					    PayPal
					  </label>
					</div>
	              {{-- <div class="custom-control custom-radio">
	                <input id="credit" name="paymentMethod" type="radio" class="custom-control-input" checked required>
	                <label class="custom-control-label" for="credit">Credit card</label>
	              </div> --}}
	             {{--  <div class="custom-control custom-radio">
	                <input id="debit" name="paymentMethod" type="radio" class="custom-control-input" checked  required>
	                <label class="custom-control-label form-control-label" for="debit">Debit card</label>
	              </div>
	              <div class="custom-control custom-radio">
	                <input id="paypal" name="paymentMethod" type="radio" class="custom-control-input" required>
	                <label class="custom-control-label mx-5" for="paypal">PayPal</label>
	              </div> --}}
	            </div>
	            <hr class="mb-4">
	            <button class="btn btn-primary btn-lg btn-block mb-5" type="submit">Continue</button>
	          </form>
	        </div>
        </div>
      </div>
    </div>
  </div>
</div>

<style type="text/css">
	label{
		font-size: 16px;
	}
</style>
<script>
      // Example starter JavaScript for disabling form submissions if there are invalid fields
      (function() {
        'use strict';

        window.addEventListener('load', function() {
          // Fetch all the forms we want to apply custom Bootstrap validation styles to
          var forms = document.getElementsByClassName('needs-validation');

          // Loop over them and prevent submission
          var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
              if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
              }
              form.classList.add('was-validated');
            }, false);
          });
        }, false);
      })();
    </script>

