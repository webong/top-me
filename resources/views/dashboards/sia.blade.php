@extends('layouts.app-dashboard')

@section('content')

@include('chrome.header-dashboard')

<section>

      @include('includes.sia-dashboard-nav-2')
      <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active py-3" id="pills-dashboard" role="tabpanel" aria-labelledby="pills-dashboard-tab">
          <!-- Body contents -->
          <div class="container container-fluid py-0 py-sm-0 py-md-2 py-lg-5 py-xl-5">

            <div class="d-flex justify-content-center justify-content-md-between bgnger">
              <div class="d-block">
                <h3 class="mb-0"> Good to have you here.</h3>
                <!-- <p><span class="sm-text">Having difficulty with navigation?</span> <span class="click-here"><a href="#">Click Here</a></span></p> -->
                <hr class="hr-line d-none d-sm-none d-md-flex d-lg-flex">
              </div>
              <div class="d-none d-sm-none d-md-block">
                <h4 class="text-right">Created Stories <a href="#"><img class= "svg1" src="img/down-arrow.svg"></a></h4>
              </div>
            </div>

            <div class="all-story bgger">
              @include('includes.sia-stories')
              <!-- <div class="d-flex justify-content-center" style="display:none">
                <nav aria-label="Page navigation example">
                  <ul class="pagination justify-content-center">
                    <li class="page-item"><a class="page-link" href="#" tabindex="-1">Prev.</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">...</a></li>
                    <li class="page-item"><a class="page-link" href="#">Next</a></li>
                  </ul>
                </nav>
              </div> -->
            </div>
          </div>

        </div>
        <div class="tab-pane fade py-3" id="pills-favourites" role="tabpanel" aria-labelledby="pills-favourites-tab">
          <!-- Body contents -->
          <div class="container container-fluid py-0 py-sm-0 py-md-2 py-lg-5 py-xl-5">

            <div class="d-flex justify-content-center justify-content-md-between">
              <div class="d-block">
                <h3> These are your favorite stories.</h3>
                <!-- <p><span class="sm-text">Having difficulty with navigation?</span> <span class="click-here"><a href="#">Click Here</a></span></p> -->
                <hr class="hr-line d-none d-sm-none d-md-flex d-lg-flex">
              </div>
              <div class="d-none d-sm-none d-md-block">
                <h4 class="text-right">Liked Stories <a href="#"><img class= "svg1" src="img/down-arrow.svg"></a></h4>
              </div>
            </div>

            <div class="all-story">
              @include('includes.fav-stories')
              <!-- <div class="d-flex justify-content-center" style="display:none">
                <nav aria-label="Page navigation example">
                  <ul class="pagination justify-content-center">
                    <li class="page-item"><a class="page-link" href="#" tabindex="-1">Prev.</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">...</a></li>
                    <li class="page-item"><a class="page-link" href="#">Next</a></li>
                  </ul>
                </nav>
              </div> -->
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="pills-create" role="tabpanel" aria-labelledby="pills-create-tab">

          @include('includes.create-story')

        </div>
        <div class="tab-pane fade" id="pills-contribution" role="tabpanel" aria-labelledby="pills-contribution-tab">

          <div class="container container-fluid py-5">

            <div class="all-story">
              @include('includes.notifications')
              <!-- <div class="d-flex justify-content-center" style="display:none">
                <nav aria-label="Page navigation example">
                  <ul class="pagination justify-content-center">
                    <li class="page-item"><a class="page-link" href="#" tabindex="-1">Prev.</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">...</a></li>
                    <li class="page-item"><a class="page-link" href="#">Next</a></li>
                  </ul>
                </nav>
              </div> -->
            </div>
          </div>

        </div>
        <div class="tab-pane fade" id="pills-settings" role="tabpanel" aria-labelledby="pills-settings-tab">

          @include('settings.sia-settings')

        </div>
      </div>
</section>


@endsection
