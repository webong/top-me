@extends('layouts.app')

@section('content')
  <div class="jumbotron jumbotron-fluid mb-0 hero text-white">
    <div class="container p-5">
      <div class=" mt-3">
        <p class="f-48 font-weight-bold my-0  w-sm-100 w-50 text-sm-jutify" style="line-height:54px; "> Create Social Impact One Life One Project. </p>
        <p class="f-16 my-3 text-sm-jutify w-sm-100 w-50"style="line-height:20px;">TOPme provides you an opportunity to commit to a worthy cause on a SECURE, CREDIBLE and ACCOUNTABLE platform</p>
        <div class="row" >
          <div class="col-12 col-md-3">
            @auth
              <a href="{{ url('/dashboard') }}">
                <button type="button" class="btn bg-white px-5 py-2 w-100  font-weight-bold btn-rounded btn-hover-purple">Back To Dashboard</button>
              </a>
            @else
              <a href="{{ url('register-as-sia') }}">
                <button type="button" class="btn bg-white px-5 py-2 w-100  font-weight-bold btn-rounded btn-hover-purple">Get Started</button>
              </a>
            @endauth

          </div>
          <div class="col-12 col-md-3 mt-sm-3 mt-3 mt-lg-0 mt-md-0">
            <a href="{{ url('/about')}}">
              <button type="button" class="btn bg-transparent px-5 py-2 border-white-alt w-100 font-weight-bold text-white btn-rounded btn-hover-white">Know Us</button>
            </a>
          </div>

        </div>
      </div>
    </div>
  </div>


<!-- <section id="Total" class="mt-0 py-0 bg-success"> -->

  @include('includes.stats')


<!-- </section> -->
<section class="my-0 my-sm-0 my-md-5 my-lg-5 my-xl-5">
  <div class="container container-fluid">
    <nav class="px-0 mx-2 border-0 d-none d-sm-none d-md-flex d-lg-flex d-xl-flex ">
      <div class="nav nav-tabs border-0 font-weight-bold" id="nav-tab" role="tablist">
        <!-- <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Home</a>
        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Profile</a>
        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Contact</a> -->
        <a class="nav-item nav-link usecase_nav active bg-white border-0 rounded-0 text-dark f-18" id="nav-all-tab" data-toggle="tab" href="#nav-all" role="tab" aria-controls="nav-all" aria-selected="true">Our Social Impact Agents (SIAs)</a>
        <a class="nav-item nav-link usecase_nav bg-white border-0 rounded-0 text-dark f-18" id="nav-trend-tab" data-toggle="tab" href="#nav-trend" role="tab" aria-controls="nav-trend" aria-selected="false">Trending</a>
        <a class="nav-item nav-link usecase_nav bg-white border-0 rounded-0 text-dark f-18" id="nav-new-tab" data-toggle="tab" href="#nav-new" role="tab" aria-controls="nav-new" aria-selected="false">Newest</a>
        <a class="nav-item nav-link usecase_nav bg-white border-0 rounded-0 text-dark f-18" id="nav-success-tab" data-toggle="tab" href="#nav-success" role="tab" aria-controls="nav-success" aria-selected="false">Most Successful</a>
        <a class="nav-item nav-link usecase_nav bg-white border-0 rounded-0 text-dark f-18" id="nav-funded-tab" data-toggle="tab" href="#nav-funded" role="tab" aria-controls="nav-funded" aria-selected="false">Highly Funded</a>
      </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="nav-all" role="tabpanel" aria-labelledby="nav-all-tab">@include('includes.all-sias')</div>
      <div class="tab-pane fade" id="nav-trend" role="tabpanel" aria-labelledby="nav-trend-tab">@include('includes.trending-sias')</div>
      <div class="tab-pane fade" id="nav-new" role="tabpanel" aria-labelledby="nav-new-tab">@include('includes.new-sias')</div>
      <div class="tab-pane fade" id="nav-success" role="tabpanel" aria-labelledby="nav-success-tab">@include('includes.successful-sias')</div>
      <div class="tab-pane fade" id="nav-funded" role="tabpanel" aria-labelledby="nav-funded-tab">@include('includes.highlyfunded-sias')</div>
    </div>
  </div>
</section>

<section id="work" class="my-4">
  <div class="jumbotron jumbotron-fluid" style="background-color:#E0E0E0">
    <div class="container">
      <p class="f-24 mb-0 text-center font-weight-bold">How TopMe works</p>
      <p class="f-18 text-center">Creating social impact is easy, follow these steps:</p>
       <div class="row mt-5 mb-3">
         <div class="col-12 col-sm-12 col-md-4 col-4 text-center my-2 my-sm-2 my-md-0 my-lg-0 my-xl-0">
            <div class=" d-flex justify-content-center mb-3">
              <div class="rounded-white-bg d-flex justify-content-center align-items-center">
                <img class="img-fluid top-work" src="img/create-campaign.svg" style="">
              </div>
            </div>
            <div class="px-3">
              <p class="f-24 font-weight-bold">1. Create a Campaign</p>
              <p class="f-18 mb-0 text-secondary" style="line-height:21px;">Lorem ipsum dolor sit ament, consectetur adipidcing elit, sed do eiusmod</p>
            </div>
         </div>

         <div class="col-12 col-sm-12 col-md-4 col-4 text-center my-4 my-sm-4 my-md-0 my-lg-0 my-xl-0">
           <div class=" d-flex justify-content-center mb-3">
             <div class="rounded-white-bg d-flex justify-content-center align-items-center">
               <img class="img-fluid top-work" src="img/share.svg" style="">
             </div>
           </div>
           <div class="px-3">
             <p class="f-24 font-weight-bold">2. Share your story</p>
             <p class="f-18 mb-0 text-secondary" style="line-height:21px;">Lorem ipsum dolor sit ament, consectetur adipidcing elit, sed do eiusmod</p>
           </div>

         </div>
        <div class="col-12 col-sm-12 col-md-4 col-4 text-center my-4 my-sm-4 my-md-0 my-lg-0 my-xl-0">
          <div class=" d-flex justify-content-center mb-3">
            <div class="rounded-white-bg d-flex justify-content-center align-items-center">
              <img class="img-fluid top-work" src="img/Group.svg" style="">
            </div>
          </div>
          <div class="px-3">
            <p class="f-24 font-weight-bold">3. Raise Fund</p>
            <p class="f-18 mb-0 text-secondary" style="line-height:21px;">Lorem ipsum dolor sit ament, consectetur adipidcing elit, sed do eiusmod</p>
          </div>
        </div>
       </div>
    </div>
  </div>
</section>

<section class="bg-waning mt-0 mb-5">
    <div class="container container-fluid ">
      <div class="row mb-3 ">
        <div class="col-12 col-sm-12 col-md-3 col-lg-3">
            <div class="d-flex justify-content-center justify-content-sm-center justify-content-md-end justify-content-lg-end justify-content-xl-end">
              <img src="{{ asset('/img/founder-alt.svg')}}" class="img-fluid rounded-circle" style=""  alt="Cinque Terre">
            </div>
        </div>

        <div class="col-12 col-sm-12 col-md-9 col-lg-9 pt-5">
          <div class="mt-3">
            <p class="mb-4 f-24 font-italic"><i class="fas fa-quote-left"></i> Anybody can do something about anything, and everybody should try. <i class="fas fa-quote-right"></i></p>
            <div class="text-right text-sm-right text-md-left text-lg-left text-xl-left mr-3 mr-sm-3 mr-md-0 mr-lg-0 mr-xl-0">
              <p  class="f-18 font-weight-bold my-0">Basil Udotai</p>
              <p class="f-14">Founder, <em>TOPme</em></p>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>





@endsection
@section('other_scripts')

<!-- <script>
var header = document.querySelector('.nav-tabs');
var btns = header.getElementsByClassName("usecase_nav");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("active-tab");
    current[0].className = current[0].className.replace(" active-tab", "");
    this.className += " active-tab";
  });
}
</script> -->

@endsection
