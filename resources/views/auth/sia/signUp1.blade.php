@extends('layouts.app')

@section('content')
<div class="container my-1 py-3 login-container bg-succss">
  <div class="d-flex justify-content-center bg-wrning px-0 pt-2 pt-sm0--2 pt-md-5 bg-dnger">
    <!-- <div class="px-0 bg-sccess  w-100-alt-diff"> -->
      <div class="card bg-uccess rounded-0 card-shadow w-100-alt-diff">
        <div class="container container-fluid p-2 p-sm-2 p-md-2 p-lg-5 p-xl-5 ">
          <div class="">
            <p class="up f-32">Sign Up
              <hr class="hr" >
            </p>
          </div>
          <div class="container container-fluid pt-5 px-0 px-sm-0 px-md-2 px-lg-5 px-xl-5 ">
            <form action="/register-as-sia" method="POST">
              @csrf
              <input type="hidden" name="step" value="1">
              <div class="form-group ">
                <input class="form-control " name="name" type="text" placeholder="Full Name" required>
              </div>
              <div class="form-group">
                <input class="form-control" name="email" type="email" placeholder="Email Address" required>
              </div>
              <div class="form-group">
                <input class="form-control " name="password" type="password" placeholder="Password" required>
              </div>
              <button type="submit" class="btn next form-control">Next</button> <br>
              <button type="button" class="btn Cancel form-control"><a class="cancel " href="/" >Cancel</a></button>  <br>
              <div class="quote">
                <p class="quotee">"Anybody can do something about <br> anything, and everyone should try".</p>
                <p class="quoter text-center text-sm-center text-md-right text-lg-right text-xl-right">-Basil Udotai</p>
              </div>
              <p class="Remin text-center text-sm-center text-md-right text-lg-right text-xl-right"> Have an account? <a style="color: rgb(116,0,194);" href="{{route('login')}}">Log In </a></p>
            </form>
          </div>

        </div>
      </div>
    <!-- </div> -->
  </div>
</div>





@endsection
