@extends('layouts.app')
@section('content')
 <div class="container center ">
        <div class="box mt-5">
            <div class="container d-block">
            <p class="level">5/5</p><br>
            <p class="up">Your account is currently being verified
            <hr class="hr">
          </p>
          <p class="verify"> Your account is currently undergoing verification, you will be notified of the <br>
                outcome through your mail. Thanks!.
        </p>
         
          </div>
        </div>         
            <p class="Remind"> Have an account? <a class="log" href="">Log In </a>
            <button type="button" class="btn Cancel form-control"><a class="cancel " href="#" >Back</a></button>           
        </p>    
      </div>

@endsection
