@extends('layouts.app')

@section('content')
<div class="container my-1 py-3 login-container bg-succss">
  <div class="d-flex justify-content-center bg-wrning px-0 pt-2 pt-sm0--2 pt-md-5 bg-dnger">
    <!-- <div class="px-0 bg-sccess  w-100-alt-diff"> -->
      <div class="card bg-uccess rounded-0 card-shadow w-100-alt-diff">
        <div class="container container-fluid p-2 p-sm-2 p-md-2 p-lg-5 p-xl-5 ">
          <div class="">
            <p class="up f-32">Sign Up
              <hr class="hr" >
            </p>
          </div>
          <div class="container container-fluid pt-5 px-0 px-sm-0 px-md-2 px-lg-5 px-xl-5 ">
            <form action="/register" method="POST">
              @csrf
              <input type="hidden" name="step" value="1">
              <div class="form-group ">
                <!-- <input class="form-control " name="name" type="text" placeholder="Full Name" required> -->
                <input id="name" type="text" placeholder="Full Name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group">
                <!-- <input class="form-control" name="email" type="email" placeholder="Email Address" required> -->
                <input id="email" type="email" placeholder="Email Address" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group">
                <!-- <input class="form-control " name="password" type="password" placeholder="Password" required> -->
                <input id="password" type="password" placeholder="Password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>
              <input type="hidden" name="role" id="role" value="individual">
              <div class="form-group">
                <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control" name="password_confirmation" required>
              </div>


              <!-- <button type="submit" class="btn next form-control">Next</button> <br> -->
              <button type="submit" class="btn next form-control">
                  {{ __('Sign Up') }}
              </button>
              <button type="button" class="btn Cancel form-control"><a class="cancel " href="/" >Cancel</a></button>  <br>
              <div class="quote">
                <p class="quotee">"Anybody can do something about <br> anything, and everyone should try".</p>
                <p class="quoter text-center text-sm-center text-md-right text-lg-right text-xl-right">-Basil Udotai</p>
              </div>
              <p class="Remin text-center text-sm-center text-md-right text-lg-right text-xl-right"> Have an account? <a style="color: rgb(116,0,194);" href="{{route('login')}}">Log In </a></p>
            </form>
          </div>

        </div>
      </div>
    <!-- </div> -->
  </div>
</div>

{{--<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <input type="hidden" name="role" id="role" value="individual">
                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>--}}
<script type="text/javascript">
    var role = document.querySelector('#role');
    var x = window.location.search.toUpperCase().substr(1);
    if (!x) {x='Individual'}
    role.value = x;

</script>
@endsection
