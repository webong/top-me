@extends('layouts.app')

@section('content')
<div class="container my-1 py-3 login-container bg-succss">
  <div class="d-flex justify-content-center bg-wrning px-0 pt-2 pt-sm0--2 pt-md-5 bg-dnger">
    <!-- <div class="px-0 bg-sccess  w-100-alt-diff"> -->
      <div class="card bg-uccess rounded-0 card-shadow w-100-alt-diff">
        <div class="container container-fluid p-2 p-sm-2 p-md-2 p-lg-5 p-xl-5 ">
          <div class="">
            <p class="up f-32">Log In
              <hr class="hr" >
            </p>
          </div>
          <div class="container container-fluid pt-5 px-0 px-sm-0 px-md-2 px-lg-5 px-xl-5 ">
            <form class=" bg-sccess " method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
              @csrf
              <div class="form-group">
                <input class= "form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" name="email"  value="{{ old('email') }}" placeholder="Email Address">
                @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
              </div>
              <div class="form-group">
                <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" placeholder="Password" required>
                @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
              </div>
              <div class="row">
                <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 bg-dnger">
                  <a href="{{ route('password.request') }}" class="text-dark">Forgot Password?</a>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-right bg-wrning">
                  <!-- Remember me -->
                  <label>
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                  </label>
                </div>
              </div>
              <button type="submit" class="btn Next form-control btn-purple-alt my-3 py-2">{{ __('Login') }}</button>
              <div class="quote">
                <p class="quotee">"Anybody can do something about <br> anything, and everyone should try".</p>
                <p class="quoter text-center text-sm-center text-md-right text-lg-right text-xl-right">-Basil Udotai</p>
              </div>

                       {{--   <span>or</span>     <br>
              <button type="button" class="btn facebook form-control"> <i class="fa fa-facebook-official" aria-hidden="true"></i> <a class="facebook " href="#" >Login with Facebook</a></button>  <br>--}}
              <p class="Remin text-center text-sm-center text-md-right text-lg-right text-xl-right">Don't have an account? <a style="color: rgb(116,0,194);" href="{{route('register')}}">Sign Up </a></p>
            </form>
          </div>

        </div>
      </div>
    <!-- </div> -->
  </div>
</div>

{{--<div class="col-md-8">
    <div class="card">
        <div class="card-header">{{ __('Login') }}</div>

        <div class="card-body">
            <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                @csrf

                <div class="form-group row">
                    <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Login') }}
                        </button>

                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>--}}
@endsection
