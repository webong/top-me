<div class="container container-fluid py-5">
  @forelse($favCampaigns as $campaign)
    @include('includes.campaigns')
  @empty
	<div class="alert alert-info">
		You don't have any favourite campaigns yet. Browse through the list of campaigns and make them favourites by liking them.
	</div>
  @endforelse
</div>
