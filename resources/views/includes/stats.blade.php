<div class="bg-light py-0 my-0 bg-dager">
  <div class="container container-fluid my-1 my-sm-1 mb-md-5 mt-md-0 mt-lg-0 mb-lg-5 py-3">
    <div class="row">
      <div class="col-6 col-md-4 text-sm-center text-center text-md-left text-lg-left">
        <p class="text-purple text-rambla f-18 my-0 font-weight-bold ml-2" style="line-height:22px;">TOTAL CAMPAIGNS:</p>
        <p class="f-64 text-roboto font-weight-bld my-0" style="line-height:75px;">{{ count($campaigns) }}</p>
      </div>
      <div class="col-6 col-md-4 text-center">
        <p class="text-purple text-rambla f-18 my-0 font-weight-bold" style="line-height:22px;">TOTAL NUMBER OF SIAS:</p>
        <p class="f-64 text-roboto font-weight-old my-0" style="line-height:75px;">{{$count_sia}}</p>
      </div>
      <div class="col-12 col-md-4 text-md-right text-lg-right text-sm-center text-center mt-3 mt-sm-3 mt-md-0 mt-lg-0">
        <p class="text-purple text-rambla text-left text-sm-left text-md-center text-lg-center f-18 my-0 font-weight-bold ml-50-alt" style="line-height:22px; margin-left:30%;">TOTAL DONORS:</p>
        <p class="f-64 text-roboto font-weight-bod my-0" style="line-height:75px;">{{$total_donors}}</p>
      </div>
    </div>
  </div>
</div>
