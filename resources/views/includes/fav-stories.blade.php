<div class="row my-3 my-sm-3 my-md-4 my-lg-5 my-xl-5" id="set">
  @forelse($favCampaigns as $campaign)
    @include('includes.campaigns')
  @empty

  @endforelse
</div>
  <script type="text/javascript">
	/* Copyright 2018 James John James
		//========================JAMES JOHN JAMES==========================\\
		The logic I wrote here is a bit too confusing and I will advocate that you use a framework if you're working on this project in the future.
		After sending the request to make the campaign a favourite, the listener which is constantly running on the background discovers the change and the tries to update the dom accordingly.
		checkForFavs is the listener, getCamps retrieves the changes, and display() shows
		it on the screen
		//=========================JAMES JOHN JAMES===========================\\
	*/

  	var interval = window.setInterval(checkForFavs, 500);
  	var current = [];

	@foreach(Auth::user()->favourites as $fav)
		current.push({{$fav->campaign_id}})
	@endforeach
	function checkForFavs(){
		var newCampaigns=[];
		// console.log(1);
		$.get('/getFavs', function(data){
			// console.log(data)
			// var response = JSON.parse(data);
            var campaigns = JSON.stringify(data.campaigns);
            var curr = JSON.stringify(current);
            var check = campaigns == curr
            // console.log(check);

            if (!check) {
            	var newCamps = campaigns.substring(1, campaigns.length-1).split(',').map(Number);

            	// console.log(newCamps)
            	current = [];
            	for (var i = 0; i < newCamps.length; i++) {
            		current.push(newCamps[i]);
            		getCamps(newCamps[i]);
            	}
            }
		});

	    function getCamps(camp){
			var result = null;
			$.get('/getfavCamp/'+camp, function(data){
				// console.log(data.length)
				newCampaigns.push(data);
				if (newCampaigns[0] == "") {
    				display("");
    			}else{
    				// console.log(newCampaigns)
					display(newCampaigns);
    			}

			});
		}
	}


	function display(camps){
		var main = document.querySelector('#set');

		if (camps == "" || camps.length == 0) {
			main.innerHTML = "";
		}

		// camps = JSON.parse(camps)
		// console.log(camps);
		main.innerHTML  = "";
		for (var i = camps.length - 1; i >= 0; i--) {
			main.innerHTML +='<div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4  mt-1 mb-3 mt-sm-1 mb-sm-3 mt-md-0 mb-md-5 mt-lg-0 mb-lg-5 mt-xl-0 mb-xl-5 d-flex justify-content-center justify-content-sm-center justify-content-md-start justify-content-lg-start justify-content-xl-start bg-succss"><div class="card pb-3 rounded-0 card-shadow campaign-card" style="width: 21rem;"><div class="text-center bg-succes px-0 rounded-0"><img class="card-img-top img-fluid clickable-card  rounded-0 mx-0 w-100" style="border-radius:0px!important; cursor: pointer;" src="'+ camps[i].pics[0].url + '" alt="Annie photo" onclick="singleStory('+camps[i].id+')" ></div><div class="card-body mb-0"><div class="border-bottom mb-3 pb-2"><h5 class="card-title ">'+camps[i].title+'</h5><p class="card-text">'+camps[i].body+'</p></div><div class="border-bottom mb-3 pb-2"><div class="progress "><div class="progress-bar prog-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div></div><div class="row"><div class="col-4 col-md-4"><label>&#8358;0<span class="donors"> raised</span></label></div><div class="col-4 col-md-4 text-center"><label><img class= "svg" src="img/user-silhouette.svg">0<span class="donors"> Donors</span></label></div><div class="col-4 col-md-4 text-right"><label><span class="donors">Total</span> &#8358;'+ camps[i].amount +'</label></div></div></div><div class="row mb-0"><div class="col-5 col-md-5"></div><div class="col-7 col-md-7"><div class="d-flex justify-content-between"><i class="fas fa-heart fa2x  a'+camps[i].id +'" id="" style="cursor: pointer;color: #8b47b0;" onclick="addToFavourites('+camps[i].id+', this)"></i><i class="fas fa-share-alt fa-2 " style="cursor: pointer;" onclick="shareModal('+camps[i].id+')"></i></div></div></div></div></div></div><button type="button" style="display: none;" class="btn btn-primary" id="share'+camps[i].id+'" data-toggle="modal" data-target="#myModall'+camps[i].id+'">not to be seen</button><!-- The Modal --><div class="modal" id="myModall'+camps[i].id+'"><div class="modal-dialog"><div class="modal-content"><!-- Modal Header --><div class="modal-header"><h4 class="modal-title">Choose A Platform to share '+camps[i].title+'</h4><button type="button" class="close" data-dismiss="modal">&times;</button></div><!-- Modal body --><div class="modal-body"><div class="row"><div class="col-12 col-md-4" style="background-color: red;"><a href="https://www.facebook.com/sharer/sharer.php?u={{route("single-story", '+camps[i].id+')}} "target="_blank">Share on Facebook</a></div><div class="col-12 col-md-4" style="background-color: red;"><a href="https://twitter.com/intent/tweet?url={{route("single-story", '+camps[i].id+')}}" target="_blank">Share on Twitter</a></div><div class="col-12 col-md-4" style="background-color: red;"><a href="https://plus.google.com/share?url={{route("single-story", '+camps[i].id+')}}"target="_blank">Share on Google plus</a></div></div></div><!-- Modal footer --><div class="modal-footer"><button type="button" class="btn btn-danger" data-dismiss="modal">Close</button></div></div></div></div>';
		}

	}
  </script>




