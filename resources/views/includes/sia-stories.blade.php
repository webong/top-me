<div class="row my-3 my-sm-3 my-md-4 my-lg-5 my-xl-5">
  @forelse($campaigns as $campaign)
    @include('includes.campaigns')
  @empty

  @endforelse

  <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4  mt-1 mb-3 mt-sm-1 mb-sm-3 mt-md-0 mb-md-5 mt-lg-0 mb-lg-5 mt-xl-0 mb-xl-5 d-flex justify-content-center justify-content-sm-center justify-content-md-start justify-content-lg-start justify-content-xl-start bg-succss">
    <div class="card  py-0 rounded-0 card-shadow" style="width: 21rem;">
      <div class="card-body bg-nger pb-0">
        <!-- <center><p class= "circle"><a href="#"><img class= "svg2 mt-4" src="img/plus-symbol.svg"></a></p></center> -->
        <div class="d-flex justify-content-center">
          <button type="button" name="button" class="btn p-4 bg-transparent border rounded-circle" id="newStory" onclick="toStory()" data-toggle="pill" href="#pills-create">
            <i class="fas fa-plus fa-3x text-secondary"></i>
          </button>
        </div>
        <div class="bg-sucs mb-0">
          <p class="text-center pt-5 add-campaign">Create A Project</p>
        </div>
      </div>
    </div>
  </div>
  
  {{-- <div class="d-flex justify-content-end"></div> --}}


</div>
<div class="d-flex justify-content-center justify-content-sm-center justify-content-md-end justify-content-lg-end justify-content-xl-end bg-succss">{!! $campaigns->render() !!}</div>
<script>
  function toStory(){
    var storyTab = document.querySelector('#pills-create-tab');
    storyTab.click();
  }
</script>
