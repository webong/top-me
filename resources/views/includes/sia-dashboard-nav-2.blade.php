<div class="d-none d-sm-none d-md-flex nav-color py-0">
  <div class="container container-fluid d-flex justify-content-center bg-sucess py-0">
    <ul class="nav mb-0 bg-dager" id="pills-tab" role="tablist">
      <li class="nav-item p-0 arning w-20">
        <a class="nav-link navitems active  py-3 px-0 mx-4" id="pills-dashboard-tab" data-toggle="pill" href="#pills-dashboard" role="tab" aria-controls="pills-dashboard" aria-selected="true">
          <!-- <img class= "svg1" src="img/dashboard_black_24px.png"> Dashboard -->
          <i class="fas fa-home text-white"></i>
          Dashboard
        </a>
      </li>
      <li class="nav-item p-0 rimary w-20">
        <a class="nav-link navitems py-3 px-0 mx-4" id="pills-favourites-tab" data-toggle="pill" href="#pills-favourites" role="tab" aria-controls="pills-favourites" aria-selected="false">
          <!-- <img class= "svg1" src="img/favorites-button.png">  -->
          <i class="fas fa-heart text-white"></i>
          Favorites
        </a>
      </li>
      <li class="nav-item p-0 econdary w-20">
        <a class="nav-link navitems py-3 px-0 mx-4" id="pills-create-tab" data-toggle="pill" href="#pills-create" role="tab" aria-controls="pills-create" aria-selected="false">
          <!-- <img class= "svg1" src="img/liked.png">  -->
          <i class="fas fa-pen-alt text-white"></i>
          Create a Story
        </a>
      </li>
      <li class="nav-item p-0 bning w-20">
        @if(count(Auth::user()->unreadNotifications)>0)
        <a class="nav-link navitems py-3 px-0 mx-4" id="pills-contribution-tab" data-toggle="pill" href="#pills-contribution" onclick="markAsRead()" role="tab" aria-controls="pills-contribution" aria-selected="false">
          <!-- <img class= "svg1" src="img/contributions.png"> -->
          <i class="fas fa-bell text-white"></i>
          Notifications <span class="badge">{{count(Auth::user()->unreadNotifications)}}</span>
          
        </a>
        @else
        <a class="nav-link navitems py-3 px-0 mx-4" id="pills-contribution-tab" data-toggle="pill" href="#pills-contribution"  style="opacity: 0.8"  role="tab" aria-controls="pills-contribution" aria-selected="false">
          <!-- <img class= "svg1" src="img/contributions.png"> -->
          <i class="fas fa-bell text-white"></i>
          Notifications
          
        </a>
        @endif
      </li>
      <li class="nav-item p-0 bght w-20">
        <a class="nav-link navitems py-3 px-0 mx-4" id="pills-settings-tab" data-toggle="pill" href="#pills-settings" role="tab" aria-controls="pills-settings" aria-selected="false">
          <!-- <img class= "svg1" src="img/settings_black_24px.png">  -->
          <i class="fas fa-gear text-white"></i>
          Settings
        </a>
      </li>
    </ul>
  </div>
</div>

<div class="d-flex d-sm-flex justify-content-center d-md-none d-lg-none d-xl-none fixed-bottom bg-sccess px-0 py-0">
  <ul class="nav m-0 bg-daner w-100" id="pills-tab" role="tablist" style="background-color:#490F83; opacity:0.8">
    <li class="nav-item p-2 bg-ning w-25">
      <a class="nav-link active navitems bgimary text-center pb-0" id="pills-dashboard-tab" data-toggle="pill" href="#pills-dashboard" role="tab" aria-controls="pills-dashboard" aria-selected="true">
        <i class="fas fa-home text-white fa-lg fa-fw"></i>
      </a>
    </li>
    <li class="nav-item p-2 bgmary w-25">
      <a class="nav-link navitems text-center pb-0" id="pills-favourites-tab" data-toggle="pill" href="#pills-favourites" role="tab" aria-controls="pills-favourites" aria-selected="false">
        <i class="fas fa-heart text-white fa-lg fa-fw"></i>
      </a>
    </li>
    <li class="nav-item p-2 bg-secondry w-25">
      <a class="nav-link navitems text-center pb-0" id="pills-create-tab" data-toggle="pill" href="#pills-create" role="tab" aria-controls="pills-create" aria-selected="false">
        <i class="fas fa-pen-alt text-white fa-lg fa-fw"></i>
      </a>
    </li>
    <li class="nav-item p-2 bg-sess  w-25">
      <a class="nav-link navitems text-center pb-0" id="pills-contribution-tab" data-toggle="pill" href="#pills-contribution" role="tab" aria-controls="pills-contribution" aria-selected="false">
        <i class="fas fa-bell text-white fa-lg fa-fw"></i>
      </a>
    </li>
    <li class="nav-item p-2 bgcondary  w-25">
      <a class="nav-link navitems text-center pb-0" id="pills-settings-tab" data-toggle="pill" href="#pills-settings" role="tab" aria-controls="pills-settings" aria-selected="false">
        <i class="fas fa-gear text-white fa-lg fa-fw"></i>
      </a>
    </li>
  </ul>
</div>
