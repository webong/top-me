<div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4  mt-1 mb-3 mt-sm-1 mb-sm-3 mt-md-0 mb-md-5 mt-lg-0 mb-lg-5 mt-xl-0 mb-xl-5 d-flex justify-content-center justify-content-sm-center justify-content-md-start justify-content-lg-start justify-content-xl-start bg-succss">
  <div class="card py-3 rounded-0 card-shadow" style="width: 19rem;">

      <div class="d-flex justify-content-center px-0 rounded-0" >
        <img class="card-img-top img-fluid mx-0 img-circle" style="border-radius:50%;" src="{{$sia->picture != null?$sia->picture: '/img/test-img.png'}}" alt="Card image cap">
      </div>
      <div class="card-body">
        <h5 class=" text-center f-18">{{ucwords($sia->name)}}</h5>
        <p class="card-text f-14">{{$sia->objective != null? $sia->objective : ucwords($sia->name) . ' is a Social Impact Agent with plans to touch the lives of lots of people.'}}
          {{-- <a href="{{ route('profile', $sia->username)}}" class="continue-btn" style="text-decoration:none!important;">Continue reading</a> --}}
        </p>
        <div class="container container-fluid">

          <div class="row">
            <div class="col"><span class="card-text sia-card-icon"><span class="fas fa-users pr-2"></span>{{ count($sia->fundedProjects()) }} Funded Projects</span></div>
            <div class="col pr-3"><span class="card-text sia-card-icon"><span class="fas fa-money px-2"></span>&#8358;{{$sia->amountRaised()}}</span></div>
          </div>

          <div class="row ">
            <div class="col"><span class="card-text sia-card-icon"><span class="fas fa-users pr-2"></span>{{count($sia->donors())}} Donors</span></div>
            <div class="col text-right"><span class="card-text sia-card-icon"></div>
          </div>

        </div>
        <center>
          <a href="{{ route('profile',$sia->username)}}"><button  type="button" class="btn btn-sm view-campaign-btn mt-3 text-purple purple-hover">View Campaigns</button></a>
        </center>
      </div>
    </div>
</div>

