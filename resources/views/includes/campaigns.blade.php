<div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4  mt-1 mb-3 mt-sm-1 mb-sm-3 mt-md-0 mb-md-5 mt-lg-0 mb-lg-5 mt-xl-0 mb-xl-5 d-flex justify-content-center justify-content-sm-center justify-content-md-start justify-content-lg-start justify-content-xl-start bg-succss">
  <div class="card pb-3 rounded-0 card-shadow campaign-card" style="width: 21rem;">
    <div class="text-center bg-succes px-0 rounded-0">
      @isset($campaign->photos[0]->url)
      <img class="card-img-top img-fluid clickable-card  rounded-0 mx-0 w-100" style="border-radius:0px!important; cursor: pointer;" src="{{$campaign->photos[0]->url}}" alt="{{$campaign->title}}" onclick="singleStory('{{$campaign->slug}}')" >
      @else
       <img class="card-img-top img-fluid clickable-card  rounded-0 mx-0 w-100" style="border-radius:0px!important; cursor: pointer;" src="{{ asset('/img/Topme Logo.png')}}" alt="{{$campaign->title}}" onclick="singleStory('{{$campaign->slug}}')" >
      @endisset
    </div>
    <div class="card-body mb-0">
      <div class="border-bottom mb-3 pb-2">
        <h5 class="card-title ">{{$campaign->title}}</h5>
        <p class="card-text">{!!strlen($campaign->body) > 90 ?substr($campaign->body, 0, 90).'...' : $campaign->body !!} 
          @if(strlen($campaign->body) > 90) 
            <a href="{{route('single-story', $campaign->slug)}}" style="color: blue;">view more</a>
          @endif
          </p>
      </div>
      <div class="border-bottom mb-3 pb-2">
        <div class="progress ">
          <div class="progress-bar prog-bar" role="progressbar" style="width: {{ $campaign->funds->sum('amount') / $campaign->amount * 100 }}%;" aria-valuenow="{{ $campaign->funds->sum('amount') / $campaign->amount * 100 }}" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        <div class="row">
          <div class="col-4 col-md-4">
            <label>&#8358;{{ number_format($campaign->funds->sum('amount')) }}<span class="donors"> raised</span></label>
          </div>
          <div class="col-4 col-md-4 text-center">
            <label><img class= "svg" src="img/user-silhouette.svg">{{count($campaign->donors())}}<span class="donors"> Donors</span></label>
          </div>
          <div class="col-4 col-md-4 text-right">
            <label><span class="donors">Total</span> &#8358;{{number_format($campaign->amount, 2, '.', ',')}}</label>
          </div>
        </div>
      </div>
      <div class="row mb-0">
        <div class="col-5 col-md-5">
          <!-- <div class=" d-flex justify-content-end">
            <button type="button" class="btn bg-transparent btn-more-alt rounded py-0">view more</button>
          </div> -->
        </div>
        <div class="col-7 col-md-7">
          <div class="d-flex justify-content-between">
            <i class="fas fa-heart fa2x  {{'a' .$campaign->id}}" id="" style="cursor: pointer; @auth {{Auth::user()->isFavourite($campaign->id)? 'color: #8b47b0;' : 'color: black'}} @else 'color: black'; @endauth"  onclick="@auth addToFavourites({{$campaign->id}}, this) @else alert('Sign In to add a story to favourite') @endauth"></i>
            <i class="fas fa-share-alt fa-2 " style="cursor: pointer;" @auth onclick="shareModal({{$campaign->id}})" @else onclick="alert('Sign in to share')" @endauth></i>
            <!-- <p class="f-12">21 days left</p> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <!-- Button to Open the Modal -->
<button type="button" style="display: none;" class="btn btn-primary" id="share{{$campaign->id}}" data-toggle="modal" data-target="#myModal{{$campaign->id}}">
  not to be seen
</button>

<!-- The Modal -->
<div class="modal" id="myModal{{$campaign->id}}">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Choose A Platform to share {{$campaign->title}}</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-12 col-md-4" style="background-color: red;">
          <a href="https://www.facebook.com/sharer/sharer.php?u={{route('single-story', $campaign->slug)}} "target="_blank">
            Share on Facebook
          </a>
        </div>
        <div class="col-12 col-md-4" style="background-color: red;">
          <a href="https://twitter.com/intent/tweet?url={{route('single-story', $campaign->slug)}}" target="_blank">
            Share on Twitter
          </a>
        </div>
        <div class="col-12 col-md-4" style="background-color: red;">
          <a href="https://plus.google.com/share?url={{route('single-story', $campaign->slug)}}"
            target="_blank">
            Share on Google plus
          </a>
        </div>
        </div>

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
