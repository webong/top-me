<div class="row my-5">
  <div class="alert alert-info">
    Frequently visited campaigns will be displayed here. You can manually add  campaigns to your dashboard by liking them
  </div>
  <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4  mt-1 mb-1 mt-sm-1 mb-sm-1 mt-md-0 mb-md-2 mt-lg-0 mb-lg-2 mt-xl-0 mb-xl-2 d-flex justify-content-center justify-content-sm-center justify-content-md-start justify-content-lg-start justify-content-xl-start bg-succss">

    <!-- <div class="card py-3 rounded-0 card-shadow" style="width: 19rem;">
      <div class="d-flex justify-content-center">
        <img class="card-img-top img-fluid" src="{{ asset('/img/test-img.png')}}" alt="Card image cap">
      </div>
      <div class="card-body">
        <h5 class=" text-center f-18"></h5>
        <p class="card-text f-14">Lorem ipsum dolor sit amet consetetur dispsicing elt, sed do eiusmod incidiunt ut laborellamco labo....<a href="{{ url('/profile')}}" class="continue-btn" style="text-decoration:none!important;">Continue reading</a></p>

        <div class="row">
          <div class="col text-center"><span class="card-text sia-card-icon"><span class="fas fa-users px-2"></span>300 Funded Projects</span></div>
          <div class="col pr-3"><span class="card-text sia-card-icon"><span class="fas fa-money px-2"></span>$4,000,000 raised</span></div>
        </div>

        <div class="row px-2">
          <div class="col"><span class="card-text sia-card-icon"><span class="fas fa-users px-2"></span>1200 Donors</span></div>
          <div class="col text-right"><span class="card-text sia-card-icon"></div>
        </div>

        <center><a href="{{ url('/profile')}}"><button  type="button" class="btn btn-sm view-campaign-btn my-3">View Campaigns</button></a></center>
      </div>
    </div> -->

    {{-- <div class="card py-3 rounded-0 card-shadow" style="width: 19rem;">
      <div class="text-center">
        <img class="card-img-top img-fluid" src="img/annie-spratt-135307-unsplash.jpg" alt="Annie's photo">
      </div>
      <div class="card-body">
        <h5 class="card-title">Kansas Water Piping</h5>
        <p class="card-text">This campaign is to allow aute irure dolor in reprehenderit in voluptate velit...</p>
        <div class="text-right">
          <label><img class= "svg" src="img/user-silhouette.svg">35<span class="donors"> Donors</span></label>
        </div>
        <div class="progress">
          <div class="progress-bar prog-bar" role="progressbar" style="width: 25%;" aria-valuenow="39" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>$3500<span class="donors"> raised</span></label>
          </div>
          <div class="col-md-6 text-right">
            <label><span class="donors">Total</span> $9000</label>
          </div>
        </div>
      </div>
    </div> --}}

  </div>
  <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4  my-1 my-sm-1 my-md-0 my-lg-0 my-xl-0 d-flex justify-content-start bg-succss">
    <div class="card  py-3 rounded-0 card-shadow" style="width: 19rem;">
      <div class="card-body">
        <!-- <center><p class= "circle"><a href="#"><img class= "svg2 mt-4" src="img/plus-symbol.svg"></a></p></center> -->
        <div class="d-flex justify-content-center">
          <button type="button" name="button" class="btn p-4 bg-transparent border rounded-circle">
            <i class="fas fa-plus fa-3x text-secondary"></i>

          </button>
        </div>
        <p class="text-center pt-5 add-campaign">Add A Project</p>
      </div>
    </div>
  </div>

</div>
