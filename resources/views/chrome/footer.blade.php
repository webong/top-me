@php
	$is_sia = false;
@endphp
@auth
	@foreach(Auth::user()->roles as $role)
		@if($role->role == 'SIA')
			@php
				$is_sia = true;
			@endphp
		@else
			@php
				$is_sia = false;
			@endphp
		@endif
	@endforeach

@else
    @php
		$is_sia = false;
	@endphp
@endauth
@if(!isset($siaReg))
	@php
		$siaReg=false;
	@endphp
@endif

@if($is_sia || $siaReg)

@else
<div class="container-fluid footer-banner pt-4 pb-1">

	<div class="container">
	  <div class="row">
	    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-white">
				<p class="f-36 text-center text-sm-center text-md-left text-lg-left">Want to be a Social Impact Agent ?</p>
	      <!-- <p>Want to be a Social Impact Agent ?</p> -->
	    </div>
	    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 pt-2">
	      <button type="button" onclick="becomeSIA()" class="btn bg-white get-started-btn w-100-alt text-purple pr-4 pl-4">Get Started</button>
	    </div>
	  </div>
	</div>

</div>
@endif
<div class="container-fluid footer-content py-0 py-sm-0 py-md-5 py-lg-5 py-xl-5">

<div class="container container-fluid pl-5 ml-5">

	<div class="row text-white d-none d-sm-none d-md-flex d-lg-flex d-xl-flex">
	    <div class="col-3">
	    	<h6 class="footer-list-title font-weight-bold">Browse Categories</h6>
	      <ul class="list-content my-4" style="opacity:0.8">
	      	<li><a href="">All Issues</a></li>
	      	<li><a href="">Campaigns</a></li>
	      	<li><a href="">Trending Campaigns</a></li>
	      	{{-- <li><a href="">International Development</a></li> --}}
	      	<li><a href="{{url('/privacy')}}">Privacy Policy</a></li>
	      	<li><a href="{{url('/privacy')}}">Terms & Services</a></li>
	      	<li><a href="{{url('/privacy')}}">Terms and Conditions</a></li>
	      </ul>
	    </div>

	    <div class="col-3">
	    	<h6 class="footer-list-title">Core Ideology</h6>
	      <ul class="list-content my-4" style="opacity:0.8">
	      	<li><a href="">Our Story</a></li>
	      	<li><a href="">Mission</a></li>
	      	<li><a href="">Vision</a></li>
	      	<li><a href="">Values/Value Statement</a></li>

	      	{{-- <h6 class="footer-list-title mt-4">How it works</h6>
	      	<li><a href="">The TopMe Academy</a></li>
	      	<li><a href="">Community Workshops</a></li>
	      	<li><a href="">Video series</a></li> --}}
	      </ul>
	    </div>

	    <div class="col-3">
	    	<h6 class="footer-list-title">Stay Updated</h6>
	      <ul class="list-content my-4" style="opacity:0.8">
	      	<li><a href="">Blog</a></li>
	      	<li><a href="">News Stories</a></li>
	      </ul>
	    </div>

	    <div class="col-3">
	    	<h6 class="footer-list-title">Help</h6>
	      <ul class="list-content my-3" style="opacity:0.8">
	      	<li><a href="">Ask the Community</a></li>
	      	<li><a href="">FAQs</a></li>

	      	<h6 class="footer-list-title mt-4">Location</h6>
	      	<li>Start Innovation Hub
	      		<br>Ibom E-libray
	      		<br>IBB way, Uyo.</li>
	      </ul>
	    </div>

	  </div>


</div>

</div>
