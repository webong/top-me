<section id="header">
  <nav class="navbar navbar-expand-lg navbar-light bg-light px-0 px-sm-1 px-md-3 px-lg-5 px-xl-5 pt-1 pb-1 pl-2 pr-3">
    <a class="navbar-brand" href="{{ url('/') }}"><img class="img-fluid" src="{{ asset('/img/Topme Logo.png')}}" alt="{{ env('APP_NAME') }}" style="height:60px; width:150px;"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item header-li">
          @auth
          <a class="nav-link" href="#"> {{Auth::user()->name}}</a>
          @else
          <a class="nav-link" href="#"></a>
          @endauth
        </li>

        <li class="nav-item header-li d-none d-sm-none d-md-none d-lg-block d-xl-block">
          <div class="btn-group">
            <button type="button" class="btn bg-transparent" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <a class="k" href="#"><img class= "header-img" src="{{Auth::user()!=null  && Auth::user()->picture != null?Auth::user()->picture: '/img/test-img.png'}}"></a>
            </button>
            <div class="dropdown-menu dropdown-menu-right rounded-0">
              @if(Auth::user() != null)
              <form method="POST" action="{{route('logout')}}" class="bg-uccess">
                @csrf
                <button style="background-color: transparent; border: none; cursor: pointer; outline: none;" class="nav-link bg-dnger w-100 text-left py-0">
                  <i class="fas fa-power-off mr-2"></i>Logout
                </button>
              </form>
              @else
              <div class="dropdown-divider"></div>
              <a href="{{url('/login')}}" class="nav-link">Login</a>
              @endif
            </div>
          </div>
        </li>
        <div class="d-block d-sm-block d-md-block d-lg-none d-xl-none">
          @if(Auth::user() != null)
          <li class="nav-item header-li">
            <form method="POST" action="{{route('logout')}}">@csrf <button style="background-color: transparent; border: none; cursor: pointer; outline: none;" class="nav-link">Logout</button></form>
          </li>
          @else
          <li class="nav-item header-li">
            <a href="{{url('/login')}}" class="nav-link">Login</a>
          </li>
          @endif
        </div>
      </ul>
    </div>
  </nav>
</section>
