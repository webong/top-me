<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkedSocialAccount extends Model
{
	protected $table = 'linked_social_accounts';
	protected $fillable = ['provider_name', 'provider_id','user_id', 'sia_registration_id','assets'];
	
	 public function user()
	{
	    return $this->belongsTo('App\User');
	}


	 public function sia()
	{
	    return $this->belongsTo('App\SiaRegistration');
	}

}
