<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentPhoto extends Model
{
    protected $fillable = ['comment_id', 'photo_id'];
}
