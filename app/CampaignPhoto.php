<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignPhoto extends Model
{
        protected $fillable = ['campaign_id', 'photo_id'];
}
