<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['campaign_id', 'user_id', 'body'];

    public function user(){
    	return $this->belongsTo('App\User');
    }
    public function campaign(){
    	return $this->belongsTo('App\Campaign');
    }

    public function photos(){
    	return $this->belongsToMany('App\Photo','comment_photos');
    }
}
