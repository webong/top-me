<?php
namespace App\Repositories;

use Auth;
use App\Photo;
use JD\Cloudder\Facades\Cloudder;
use App\CampaignPhoto;
use App\CommentPhoto;
/**
 * 
 */
class PhotoRepository 
{
	
	function __construct()
	{
		# code...
	}

	public function upload($photos){
		$url = $this->uploadToCloudinary($photos);
		return $url;
	}
	public function uploadToCloudinary($photo){
		$pic = $photo->getRealPath();
        Cloudder::upload($pic, null);
        $picture =Cloudder::show(Cloudder::getPublicId(),["width"=>500, "height"=> 500]);
        $picture = 'https'. substr($picture, 4);
        return $picture;
	}
	public function save($picture, $campaign){
		$photo = new Photo;
		$photo->url = $picture;
		$photo->uploaded_by = Auth::user()->id;
		$photo->save();

		CampaignPhoto::create([
			'campaign_id'=>$campaign->id,
			'photo_id'=>$photo->id
		]);
		return true;
	}

	public function saveComment($picture, $comm){
		$photo = new Photo;
		$photo->url = $picture;
		$photo->uploaded_by = Auth::user()->id;
		$photo->save();

		CommentPhoto::create([
			'comment_id'=>$comm->id,
			'photo_id'=>$photo->id
		]);
		return true;
	}
}