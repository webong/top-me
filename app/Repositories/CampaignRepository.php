<?php
namespace App\Repositories;

use App\Campaign;
use Illuminate\Http\Request;
use Auth;
use App\Repositories\PhotoRepository;
use App\Favourite;
use App\Comment;
/**
 * 
 */
class CampaignRepository
{
	protected $photos;
	
	function __construct()
	{
		$this->photos = new PhotoRepository;
	}

	public function createCampaign(Request $request){
		
		$campaign = Campaign::create([
			'author'	=>	Auth::user()->id,
			'title'		=>	$request->title,
			'body'		=>	$request->body,
			'amount'	=>	$request->amount,
			'slug'	=>	$this->getslug($request->title),
		]);
		$pictures = explode(', ', substr($request->pictures, 2));
		foreach ($pictures as $picture) {
			$picture = 'https'. substr($picture, 4);
			$this->photos->save($picture, $campaign);
		}
		// if($request->image != null ){
		// 	if (count($request->file('image')) >= 3) {
		// 		$pictures = $this->photos->upload($request->file('image')[0]);
		// 		$this->photos->save($pictures, $campaign);

		// 		$pictures = $this->photos->upload($request->file('image')[1]);
		// 		$this->photos->save($pictures, $campaign);

		// 		$pictures = $this->photos->upload($request->file('image')[2]);
		// 		$this->photos->save($pictures, $campaign);
		// 	}else{
		// 		foreach ($request->file('image') as $image) {
		// 			$pic = $this->photos->upload($image);
		// 			$this->photos->save($pic, $campaign);
		// 		}
		// 	}
			

		// }

		return $campaign;
	}
	public function getSiaCampaigns(){
		$campaigns = Campaign::where('author', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(8);

		return $campaigns;
	}

	public function getAll(){
		return Campaign::all();
	}

	public function addToFav($campaign){
		$fav = Favourite::where('user_id', Auth::user()->id)->where('campaign_id', $campaign->id)->first();
		if ($fav == null) {
			$fav = Favourite::create([
				'user_id'=> Auth::user()->id,
				'campaign_id'=>$campaign->id
			]);
			if($fav){
				return true;
			}else{
				return false;
			}
		}else{
			$fav->delete();
			return true;
		}
		
	}

	public function getFav(){
		$favs = Favourite::where('user_id', Auth::user()->id)->get();
		$farv = [];
		foreach ($favs as $fav) {
			array_push($farv, Campaign::find($fav->campaign_id));
		}
		$farv = collect($farv);
		return $farv;
	}

	public function getCampaigns($user){
		$camps = Campaign::where('author', $user->id)->orderBy('created_at', 'desc')->paginate(9);
		return $camps;
	}

	public function comment($campaign, $request){
		$comm = Comment::create([
			'campaign_id'		=>		$campaign,
			'user_id'			=>		Auth::user()->id,
			'body'				=>		$request->body
		]);

		if ($request->hasFile('image')) {
			// dd($request->file('image') );
			foreach ($request->file('image') as $image) {
				$pictures = $this->photos->upload($image);
				$this->photos->saveComment($pictures, $comm);
			}
			
		}
		// dd($request);
		return $comm;
	}

	public function getslug($title)
    {
 	$remove      =  array("?","%","$","@","*", "\/", "\\", "!", "(", ")", "#", "^", "+", "=", '.', ",", "'", ">", "<", "]", "[", '{', "}", ";", ":", '"' );
 	$string      = str_replace( $remove , "", $title);
 	$str         = strtolower($string);
 	$rep         = preg_replace('/\s+/', '-', $str);
 	$characters  = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
 	$date = new \DateTime;
	$slug = $rep. '-' . str_shuffle($date->getTimestamp());
       
   	return $slug;
    }
}