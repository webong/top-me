<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CampaignRepository;
use App\Campaign;
use App\Photo;
use Alert;
use Auth;
use App\Notifications\CampaignLiked;
use App\Notifications\CampaignComments;

class CampaignController extends Controller
{
	protected $campaigns;

	function __construct(){
		$this->campaigns = new CampaignRepository;
	}

    public function create(Request $request){
        if ($request->pictures== null || $request->pictures == "") {
            Alert::error('No file selected', 'Campaign not Created')->autoclose(2000);
            return back();

        }else{
            $campaign = $this->campaigns->createCampaign($request);
            // dd($campaign);
            Alert::success('Successfully created campaign', 'Campaign Created')->autoclose(2000);
            return back();  
        }
    	
    	//redirect to the campaigns page
    }

    public function show($slug, Request $request){
        $back = $request->session()->get('_previous')['url'];

        $camp = Campaign::where('slug', $slug)->first();
        if ($camp == null) {
            return abort(404);
        }
        $otherCampaigns = $camp->creator->campaigns->where('id', '<>', $camp->id);
        // dd($otherCampaigns);
        $comments = $camp->comments->reverse();
        return view('includes.single-story', [
            'campaign'          =>  $camp,
            'back'              =>  $back,
            'comments'          =>  $comments,
            'otherCampaigns'    =>  $otherCampaigns->take(2)
        ]);
    }

    public function makeFavourite(Campaign $campaign){
        if($this->campaigns->addToFav($campaign)){
            if (Auth::user()->id != $campaign->creator->id) {
                $campaign->creator->notify(new CampaignLiked($campaign->id, Auth::user()->id));
            }
            
            return response()->json(['result'=>'successful']);
        }else{
            return response()->json(['result'=>'unsuccessful']);
        }

    }

    public function getFavs(){
        // return 'james';
        $favs = Auth::user()->favourites;
        $campaigns = [];
        foreach ($favs as $fav) {
            array_push($campaigns, $fav->campaign_id);
        }
        return response()->json(['campaigns'=>$campaigns]);
    }

    public function getCamp($camp){
        if ($camp == 0) {
            return null;
        }
        $campaign = Campaign::find($camp);
        $campaign->pics = $campaign->photos;
        // dd($campaign->photos);
        return $campaign;  

    }

    public function comment($campaign, Request $request){

        $comment = $this->campaigns->comment($campaign, $request);
        if ($comment) {
            if (Auth::user()->id != Campaign::find($campaign)->creator->id ) {
                Campaign::find($campaign)->creator->notify(new CampaignComments($campaign, Auth::user()->id, $comment->id));
            }
            
            Alert::success('comment successfully', 'Comment created')->autoclose(2000);
            return back();
        }
    }

    // public function test(){
    //     dd(Photo::all());
    // }
}
