<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Repositories\UserRepository;
use App\Repositories\CampaignRepository;
use App\User;
use App\Repositories\FundsRepository;
use Alert;

class UserController extends Controller
{
	protected $users;
    protected $campaigns;
    protected $funds;

	function __construct(){
        $this->campaigns = new CampaignRepository;
		$this->users = new UserRepository;
        $this->funds = new FundsRepository;
	}

    public function becomeSIA(){
    	// dd(Auth::user());
    	if (Auth::user() != null) {
    		if($this->users->makeSIA(Auth::user()->id)){
	    		return 'successful';
	    	}else{
	    		return 'unsuccessful';
	    	}
    	}
    	else{
    		return 'redirect to register';
    	}

    }

    public function showDashboard(){
        if (Auth::user() != null) {
            if($this->users->isSIA(Auth::user())){
                $favCampaigns = $this->campaigns->getFav();
                $campaigns = $this->campaigns->getSiaCampaigns();
								// dd(Auth::user()->roles);
                return view('dashboards.sia', [
                    'campaigns'     =>      $campaigns,
                    'favCampaigns'  =>      $favCampaigns
                ]);
            }else{

                $favCampaigns   =   $this->campaigns->getFav();
                $campaigns      =   [];
                return view('dashboards.user', [
                    'favCampaigns'      =>      $favCampaigns,
                    'campaigns'         =>      $campaigns
                ]);
            }

        }
        else{
            return redirect('/login');
        }
    }

    public function profile($username){
        $user = User::where('username', $username)->first();
        if ($user == null) {
            return abort(404);
        }
        $sias = $this->users->getAllSia()->where('id', '<>', $user->id);
        if (Auth::user()!= null) {
            $sias = $sias->where('id', '<>', Auth::user()->id);
        }
        // dd($sias);
        $count_sia = count($sias);
        $count_campaigns = count($user->campaigns);
        $allCampaigns = $this->campaigns->getCampaigns($user);
        $total_donors = count($this->funds->allDonors());
        // dd($count_sia);
        return view('profile', [
            'count_sia'         =>  $count_sia,
            'campaigns'         =>  $allCampaigns,
            'total_donors'      =>  $total_donors,
            'user'              =>  $user,
            'sias'              =>  $sias,
            'count_campaigns'   =>  $count_campaigns
        ]);
    }

        public function update(Request $request){
        if($this->users->update($request)){
            Alert::success('Successfully updated profile')->autoclose(2000);
            return back();
        }else{
            Alert::error('Unable to complete at this time. Please try again.')->autoclose(2000);

            return back();
        }
    }
}
