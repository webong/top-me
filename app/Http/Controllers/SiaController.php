<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use Auth;
use Alert;

class SiaController extends Controller
{
	protected $sias;

	function __construct(){
		$this->sias = new UserRepository;
	}

	public function showRegister(Request $request){
		$step = $request->step;
		// dd($step);
		if ($step == null) {
			$step = 1;
		}
		if ($step != 1) {
			//for test
			if($step ==2){return redirect('/dashboard');}
			
			if (!$request->session()->has('sia')) {
				$step = 1;
				return redirect("/register-as-sia?step=".$step);
			}
		}
		if (auth::user() != null) {
			$status = $this->sias->hasStartedSiaRegistration(auth::user());
			if($status){
				if ($status['step'] == 6) {
					return redirect('/dashboard');
				}else{
					return redirect("/register-as-sia?step=".$status['step']);
				}
			}
			$sia = $this->sias->userBecomeSia(auth::user());
			$request->session()->put('sia', $sia);
			$step = 2;
		}
		$user = $request->session()->get('sia', null);
		$siaReg = true;
		return view('auth.sia.signUp'.$step, ['user'=>$user, 'siaReg'=>$siaReg]);
	}

	public function register(Request $request){
		$step = $request->step;
		if ($step == null) {
			$step = 1;
		}
		// dd($step);
		if ($step == 1) {
			if ($this->sias->hasStartedSiaRegistration($request)) {
				$data = $this->sias->hasStartedSiaRegistration($request);
				 // dd($data);
				if (!$request->session()->has('sia')) {

					$request->session()->put('sia', $data['sia']);	
				}else{
					$sia = $request->session()->get('sia');
					if ($sia->email != $data['sia']->email) {
						$request->session()->put('sia', $data['sia']);
					}
				}

				//step6 has finished registration as sia
				if ($data['step'] != 6) {
					return redirect("/register-as-sia?step=".$data['step']);
				}else{
					Auth::login($data['user']);
					return redirect ('/dashboard');
				}
				
			}
		}else{
			//for test purposes
			return redirect('/login');
		}



		$data = $this->sias->registerSia($request, $step);
			//test purposes
		$user = $this->sias->registerUser($data['sia']);
		// dd($user);
		Auth::login($user);
		return redirect ("/dashboard");
		//end of test
		if (!$request->session()->has('sia')) {
			$request->session()->put('sia', $data['sia']);	
		}else{
			$sia = $request->session()->get('sia');
			if ($sia->email != $data['sia']->email) {
				$request->session()->put('sia', $data['sia']);
			}
		}

		//check if the user has finished registration
		if(isset($data['user'])){
			// dd($data['user']);
			Auth::login($data['user']);
			return redirect('/dashboard');
		}
		//step6 has finished registration as sia
		if ($data['step'] != 6) {
			return redirect("/register-as-sia?step=".$data['step']);
		}else{
			return redirect('/dashboard');
		}

	}

    public function allSias(Request $request){
    	//working on search
    	if ($request->search != null) {
    		$results = $this->sias->searchSia($request->search);
    		return view('listing', [
    			'sias' 		=> 		$results,
    			'query'		=>		$request->search,
    			'total'		=>		count($results)
    		]);
    	}
    	$sias = $this->sias->getAllSia();
    	return view('listing')->with('sias', $sias);
    }

    public function verifyEmail($token, $id){
    	$verify = $this->sias->verifySiaEmail($token, $id);
    	if ($verify) {
    		session()->put('sia', $verify);
			/* Removing all the complex methods of authentication and making it simple 
			without all the bvn, twitter hustles and bustles.  */

			$user = $this->sias->registerUser($verify);
			Auth::login($user);
    		return redirect ("/dashboard");
    	}else{
    		return 'An error occured please try again';
    	}
    }


}
