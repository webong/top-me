<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SocialAccountController extends Controller
{
       /**
     * Redirect the user to the authentication page.
     *
     * @return Response
     */
    public function redirectToProvider(Request $request,$provider)
    {

      
    	//dd( url('register-as-sia?step=4'));
    	$request->session()->put('step4', url('register-as-sia?step=4'));
        return \Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information
     *
     * @return Response
     */
    public function handleProviderCallback(Request $request,\App\SocialAccountService $accountService, $provider)
    {

	      try {
	          $user = \Socialite::with($provider)->user();
	        }
	         catch (\Exception $e) 
	         {
	             return redirect()->back();
	            //dd($e);
	        }

	        $asset = json_encode(['image_url' => $user->avatar_original, 'username' => $user->nickname ]);
           
           //dd($asset);

	        $authUser = $accountService->findOrCreate(
	            $user,
	            $provider,
	            $asset
	        );

	        //dd($authUser);

            $url = $request->session()->get('step4');
	        //return[ 'user' => $user, 'provider' => $provider ];

	        //auth()->login($authUser, true);

	        return redirect($url);
	}
}
