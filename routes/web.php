<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::post('/donate', 'DonateController@donate');
Route::get('/donate/callback', 'DonateController@handleGatewayCallback');

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/', 'HomeController@index');


Route::get('/about', function () {
    return view('about');
});
Route::get('/campaigns/{slug}', 'CampaignController@show')->name('single-story');
Route::get('/impact-agents', 'SiaController@allSias')->name('all-sias');

Route::get('/privacy', function () {
    return view('pages.pages');
});

Route::get('/terms&condition', function () {
    return view('terms&condition');
});

Route::get('/terms-of-service', function () {
    return view('terms-of-service');
});

Route::get('/dashboard', 'UserController@showDashboard');
Route::get('/signup4', function () {
    return view('signUp4');
});
Route::get('/signUp5', function () {
    return view('signUp5');
});
Route::get('/register-as-sia', 'SiaController@showRegister');
Route::post('/register-as-sia', 'SiaController@register');
Route::get('/sia/verify/{token}/{id}', 'SiaController@verifyEmail');
Route::get('/sia-verify-login/{provider}', 'Auth\SocialAccountController@redirectToProvider');
Route::get('/sia-verify-login/{provider}/callback','Auth\SocialAccountController@handleProviderCallback');
Auth::routes();

Route::get('/home', 'HomeController@home')->name('home');


Route::post('/create-campaign', 'CampaignController@create');


Route::post('/update-bio', 'UserController@update');

//About Favourites
Route::get('/addToFavourite/{campaign}', 'CampaignController@makeFavourite');
Route::get('/getFavs/', 'CampaignController@getFavs');
Route::get('/getfavCamp/{camp}', 'CampaignController@getCamp');


Route::get('/sia/{username}', 'UserController@profile')->name('profile');
Route::post('/{campaign}/comment', 'CampaignController@comment')->name('comment');

Route::get('/markAsRead', function() {
    Auth::user()->unreadNotifications->markAsRead();
});
// Route::get('/allPhotos', 'CampaignController@test');

